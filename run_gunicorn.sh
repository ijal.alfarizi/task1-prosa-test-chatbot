#!/bin/sh
export $(xargs < .env.dev)
gunicorn --bind 0.0.0.0:${APP_PORT} --worker-class eventlet --workers 1 --worker-connections 2000 --timeout 6000 bin.run:flask_app