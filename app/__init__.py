from flask import Flask
from flask_socketio import SocketIO
from app.web.chat.views import ChatController
from .bot import ChatbotLoader
from .apis.telegram_bot.views import register_route
import logging

cb = ChatbotLoader('app/bot/dataset/conversation.txt')
logging.basicConfig(level=logging.DEBUG)


def create_app():
    app = Flask(__name__, instance_relative_config=True,
                template_folder='../app/web/templates')
    app.config['SECRET_KEY'] = 'some-secret'

    socketio = SocketIO(app)
    
    cb.train()

    # web 
    ChatController.register(app, route_base='/chat')

    # api
    app = register_route(app, cb)


    @socketio.on('chatbot')
    def handle_message(data, methods=['GET', 'POST']):
        # logging.debug(data)
        if data:
            bot_msg = cb.inference(data['message'])
            # print(bot_msg)
            ret = {
                'message': str(bot_msg)
            }

            socketio.emit('send-message', ret)

    return app, socketio
