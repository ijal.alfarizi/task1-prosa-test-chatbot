from flask import render_template
from flask_classful import FlaskView


class ChatController(FlaskView):

    def index(self):
        return render_template("chat/index.html")
