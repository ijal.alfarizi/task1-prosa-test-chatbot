
import telegram
from app.config import *
from flask import request

def register_route(app, cb):
    TOKEN = BOT_TOKEN
    tele_bot = telegram.Bot(TOKEN)

    @app.route('/{}'.format(TOKEN), methods=['POST'])
    def respond():
        # retrieve the message in JSON and then transform it to Telegram object
        update = telegram.Update.de_json(request.get_json(force=True), tele_bot)

        chat_id = update.message.chat.id
        msg_id = update.message.message_id

        # Telegram understands UTF-8, so encode text for unicode compatibility
        text = update.message.text.encode('utf-8').decode()
        print("got text message :", text)

        response = cb.inference(text)
        tele_bot.sendMessage(chat_id=chat_id, text=str(response), reply_to_message_id=msg_id)

        return ''

    @app.route('/setwebhook', methods=['GET', 'POST'])
    def set_webhook():
        s = tele_bot.setWebhook('{URL}{HOOK}'.format(URL=HEROKU_APP, HOOK=TOKEN))
        if s:
            return "webhook setup ok"
        else:
            return "webhook setup failed"

    return app