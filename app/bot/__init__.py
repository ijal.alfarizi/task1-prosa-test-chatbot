from chatterbot import ChatBot
from chatterbot.trainers import ListTrainer, ChatterBotCorpusTrainer


class ChatbotLoader:

    def __init__(self, dataset_file):
        with open(dataset_file, 'r') as file:
            self.data = file.readlines()
            self.data = [d.replace('\n', '') for d in self.data]
            
        self.cb = ChatBot(
            'chatbot',
            logic_adapters=[
                {
                    'import_path': 'chatterbot.logic.BestMatch',
                    'default_response': 'I am sorry, but I do not understand.',
                    'maximum_similarity_threshold': 0.5
                }, { 
                    'import_path': 'chatterbot.logic.MathematicalEvaluation',
                }
            ]
        )

    def train(self):
        
        trainer = ListTrainer(self.cb)
        trainer.train(self.data)
        # trainer = ChatterBotCorpusTrainer(self.cb)
        # trainer.train(
        #     "chatterbot.corpus.english.greetings",
        #     "chatterbot.corpus.english.conversations"
        # )

    def inference(self, message):
        return self.cb.get_response(message)
