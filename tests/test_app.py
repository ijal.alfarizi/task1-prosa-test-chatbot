import unittest
from app import create_app
from flask import Flask
from flask_socketio import SocketIO
from unittest.mock import patch, MagicMock


class ChatbotMock():

    def train(self):
        return "Mocked"

    def inference(self):
        return "Mocked"

class TelegramChatMock():
    id = 'tes_id'

class TelegramMessageMock():
    chat = TelegramChatMock()
    message_id = 'tes_m'
    text = MagicMock(return_value='text')


class TelebotMock():

    def sendMessage(self,* args):
        print('mocked')
        pass


class TelegramMock():
    message = TelegramMessageMock()

    def Bot(self, token):
        return TelebotMock()


class AppTest(unittest.TestCase):

    def setUp(self):
        chatbot_train_mock = patch("app.cb").start() 
        chatbot_train_mock.return_value = ChatbotMock()
        
        self.ctx_app, self.ctx_socket = create_app()
        self.client = self.ctx_app.test_client()

    def test_create_app(self):

        self.assertTrue(self.ctx_app)
        self.assertTrue(self.ctx_socket)

        self.assertTrue(isinstance(self.ctx_app, Flask))
        self.assertTrue(isinstance(self.ctx_socket, SocketIO))
    
    def test_socket_connect(self):
        client = self.ctx_socket.test_client(self.ctx_app)
        self.assertTrue(client.is_connected)

    def test_send_message(self):
        # socketio = SocketIO(self.ctx_app, logger=True, engineio_logger=True)
        client = self.ctx_socket.test_client(self.ctx_app, namespace='/')
        client.get_received('/')

        client.emit('chatbot', {'message': 'test'})
        
        received = client.get_received('/')
        self.assertEqual(len(received), 1)
        self.assertEqual(len(received[0]['args']), 1)

    # def test_telgram_endpoint(self):
    #     TOKEN = '1249682113:AAHaURUUSE8OAsoo0ez_4GERoftWbNYblgc'

    #     patch_telegram = patch('app.apis.telegram_bot.views.telegram.Update').start()
    #     patch_telegram.return_value = MagicMock()

    #     patch_telegram = patch('app.apis.telegram_bot.views.telegram.Bot').start()
    #     patch_telegram.return_value = MagicMock()
        
    #     req = self.client.post('/{}'.format(TOKEN), json={'tes': 'sample'})
    #     res = req.get_data()
    #     print(res)

if __name__ == "__main__":
    unittest.main()