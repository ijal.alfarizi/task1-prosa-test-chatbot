import unittest
from unittest.mock import patch
from app.bot import ChatbotLoader


class ChatbotLoaderTest(unittest.TestCase):

    def setUp(self):
        self.cb = ChatbotLoader('tests/data/test_data.txt')
        mock_chatterbot_train = patch('app.bot.ListTrainer.train').start()
        mock_chatterbot_train.return_value = "mocked"
        mock_chatterbot_get_response = patch('app.bot.ChatBot.get_response').start()
        mock_chatterbot_get_response.return_value = "mocked"
        
    def test_train(self):
        try:
            self.cb.train()
        except Exception as e:
            self.fail()
    
    def test_get_response(self):
        self.assertEqual(self.cb.inference('test'), 'mocked')   

if __name__ == "__main__":
    unittest.main()
