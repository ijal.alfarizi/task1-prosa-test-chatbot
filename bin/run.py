
import os, sys
lib_path = os.path.abspath('./app')
sys.path.insert(0, lib_path)

print(sys.path)

from app import create_app

flask_app, socket = create_app()

if __name__ == "__main__":
    socket.run(flask_app, debug=True)
