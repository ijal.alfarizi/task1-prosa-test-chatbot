## **Chatbot App**

This project is web app that provides chatbot with Web socket and Chatterbot lib.

dataset is sample conversation in english stored in txt file in `app/bot/dataset` folder

run app:
 1. via flask: execute bash script `./run_flask.sh`
 2. via gunicorn: execute bash script `./run_gunicorn.sh`
 3. via docker: execute `source ./run_docker.sh`
 4. route to `/chat` to see chatbot UI

live app is available on : https://rizal-chatbot.herokuapp.com/chat/
please wait until the server is up again because heroku free account will deactive after 30mins inactivity.

if the server is up you can try my bot on telegram, just search @ChatbotRizal

You can try typing for example:
"hello"
"hi"
"what is 2 + 2 ?"
"i need a job"
"i have interview schedule"
"good morning"
"i like to run"

run test : execute bash script `./run_test.sh`

